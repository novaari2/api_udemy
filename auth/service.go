package auth

import (
	"database/sql"
	"encoding/base64"
	"fmt"
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
)

type User struct {
	Id        int
	Username  string
	Password  string
	CreatedAt *time.Time
	UpdatedAt *time.Time
}

func SetupAuth() fiber.Handler {
	return func(c *fiber.Ctx) error {
		auth := c.Get("Authorization")

		if auth == "" {
			c.Status(fiber.StatusUnauthorized)
			return c.JSON(fiber.Map{
				"message": "Authentikasi Header Tidak Ditemukan",
			})
		}

		decode, err := base64.StdEncoding.DecodeString(auth[len("Basic "):])
		if err != nil {
			c.Status(fiber.StatusUnauthorized)
			return c.JSON(fiber.Map{
				"message": "Invalid Authorization in header value",
			})
		}

		creds := string(decode)
		fmt.Println(creds)
		fmt.Println(creds)
		username := creds[:len(creds)-7]
		fmt.Println(username)
		password := creds[7:]
		fmt.Println(password)

		db, err := sql.Open("postgres", "host=localhost port=5432 user=postgres password='' dbname=api-udemy sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}

		defer db.Close()

		row := db.QueryRow("SELECT username, password FROM user WHERE username= $1 AND password = $2", username, password)
		user := User{}
		fmt.Println(row)
		err = row.Scan(&user.Username, &user.Password)
		if err != nil {
			c.Status(fiber.StatusUnauthorized)
			return c.JSON(fiber.Map{
				"message": "Invalid Username or Password",
			})
		}

		if user.Password != password {
			c.Status(fiber.StatusUnauthorized)
			return c.JSON(fiber.Map{
				"message": "Invalid username or password",
			})
		}

		// c.Context().SetUserValue("user_id", user.Id)

		return c.Next()
	}
}
