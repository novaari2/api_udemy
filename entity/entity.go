package entity

import "time"

type Course struct {
	Id           int
	Title        string
	Subtitle     string
	Creator      string
	Language     string
	Price        float64
	WhatYouLearn string
	CreatedDate  *time.Time
}
