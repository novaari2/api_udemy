package main

import (
	"log"
	"udemyapi/api"
	"udemyapi/auth"

	"github.com/gofiber/fiber/v2"
	_ "github.com/lib/pq"
)

func SetupRoutes(app *fiber.App) {
	app.Get("api/course", api.GetData)
	app.Get("api/login", auth.SetupAuth(), func(c *fiber.Ctx) error {
		return c.SendString("You are authenticated")
	})
}

func main() {
	app := fiber.New()

	SetupRoutes(app)

	log.Fatal(app.Listen(":3000"))
}
