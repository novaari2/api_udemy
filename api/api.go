package api

import (
	"database/sql"
	"udemyapi/entity"

	"github.com/gofiber/fiber/v2"
)

const (
	host     = "localhost"
	port     = 5432
	password = ""
	user     = "postgres"
	dbname   = "api-udemy"
)

func GetData(c *fiber.Ctx) error {
	db, err := sql.Open("postgres", "host=localhost port=5432 user=postgres password='' dbname=api-udemy sslmode=disable")

	if err != nil {
		return err
	}
	defer db.Close()

	rows, err := db.Query("SELECT id, title, subtitle, creator, language, price, whatyoulearn, created_date FROM course")

	if err != nil {
		return c.Status(500).SendString(err.Error())
	}
	defer rows.Close()

	result := []entity.Course{}

	for rows.Next() {
		courses := entity.Course{}
		err := rows.Scan(&courses.Id, &courses.Title, &courses.Subtitle, &courses.Creator, &courses.Language, &courses.Price, &courses.WhatYouLearn, &courses.CreatedDate)

		if err != nil {
			return c.Status(500).SendString(err.Error())
		}

		result = append(result, courses)
	}

	return c.JSON(result)
}
